<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>lahtihan</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="berita.html">Berita</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="galeri.html">Glaleri</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-8">
            <div class="table-responsive">
                <table>
                    <tbody>
                        <tr>
                            <td>NPM</td>
                            <td>:</td>
                            <td>07122005</td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td>lee chen kho </td>
                        </tr>
                        <tr>
                            <td>jenis</td>
                            <td>:</td>
                            <td>Laki-laki </td>
                        </tr>
                        <tr>
                            <td>tempat tangal lahir</td>
                            <td>:</td>
                            <td>baturaja, 27 desmseber 2002 </td>
                        </tr>
                        <tr>
                            <td>alamat </td>
                            <td>:</td>
                            <td>jl letnan hadin, RT 029, RW 010, DESA 20 lilir, KEC lilr timur 1, kota palembang</td>
                        </tr>
                        <tr>
                            <td>Nomor hp  </td>
                            <td>:</td>
                            <td>088779642708</td>
                        </tr>
                        <tr>
                            <td>HOBI </td>
                            <td>:</td>
                            <td>dengar musik, memasak </td>
                        </tr>
                        <tr>
                            <td>sekolah </td>
                            <td>:</td>
                            <td>SD negeri 2 GUMAWANG, SMPN 2 BELITANG, SMAN 14 PALEMBANG </td>
                        </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>